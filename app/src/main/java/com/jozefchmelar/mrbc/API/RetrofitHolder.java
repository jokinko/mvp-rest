package com.jozefchmelar.mrbc.API;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jojko on 10.10.2016.
 */
public class RetrofitHolder {
    public static final String BASE_URL = ""; //privatna rest apina
    private static RetrofitHolder ourInstance;
    private static Retrofit retrofit;
    private static Gson gson;

    private RetrofitHolder() {
          this.gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
          this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static synchronized IRoutes getClient(){
        if(retrofit==null){
            ourInstance = new RetrofitHolder();
            return ourInstance.retrofit.create(IRoutes.class);
        }
        else return ourInstance.retrofit.create(IRoutes.class);
    }
}
