package com.jozefchmelar.mrbc.API.POJO.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jojko on 26.10.2016.
 */

public class Order {
    private List<Item> items = new ArrayList<Item>();
    private String kind;
    private String etag;

    /**
     * @return The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * @param kind The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * @return The etag
     */
    public String getEtag() {
        return etag;
    }

    /**
     * @param etag The etag
     */
    public void setEtag(String etag) {
        this.etag = etag;
    }




    public class Item {

        private Id id;
        private String name;
        private Integer count;
        private String kind;

        /**
         * @return The id
         */
        public Id getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Id id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The count
         */
        public Integer getCount() {
            return count;
        }

        /**
         * @param count The count
         */
        public void setCount(Integer count) {
            this.count = count;
        }

        /**
         * @return The kind
         */
        public String getKind() {
            return kind;
        }

        /**
         * @param kind The kind
         */
        public void setKind(String kind) {
            this.kind = kind;
        }

    }



}
