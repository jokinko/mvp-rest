package com.jozefchmelar.mrbc.API.POJO.Contact;

/**
 * Created by jojko on 26.10.2016.
 */

public class NewContact {
    private String name;
    private String phone;

    public NewContact(String name, String phoneNumber) {
        this.name=name;
        this.phone=phoneNumber;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

}
