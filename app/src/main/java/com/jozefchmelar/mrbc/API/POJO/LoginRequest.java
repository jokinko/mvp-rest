package com.jozefchmelar.mrbc.API.POJO;

/**
 * Created by jojko on 10.10.2016.
 */

public class LoginRequest {
    private String email;
    private String password;

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
