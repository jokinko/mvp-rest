package com.jozefchmelar.mrbc.API;

import com.jozefchmelar.mrbc.API.POJO.Contact.Contact;
import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.API.POJO.Contact.NewContact;
import com.jozefchmelar.mrbc.API.POJO.Order.Order;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by jojko on 10.10.2016.
 */
public interface IRoutes {
    @GET("contactendpoint/v1/contact/")
    Call<Contact> getContacts();

    @GET("orderendpoint/v1/order/{id}")
    Call<Order> getOrder(@Path("id") String id);

    @POST("contactendpoint/v1/contact/")
    Call<Item> newContact(@Body Item newContact);

}
