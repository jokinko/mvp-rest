package com.jozefchmelar.mrbc.API.POJO.Contact;

import java.io.Serializable;

/**
 * Created by jojko on 26.10.2016.
 */

public class Item implements Serializable {

    private String id;
    private String name;
    private String phone;
    private String kind;

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", kind='" + kind + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                '}';
    }

    public Item(String name, String phoneNumber) {
        this.name=name;
        this.phone=phoneNumber;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String pictureUrl;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name==null ? "no name":name.trim();
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone ==null ? "no phone":phone.trim();
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     *
     * @param kind
     * The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }
}
