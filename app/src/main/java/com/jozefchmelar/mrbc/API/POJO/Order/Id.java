package com.jozefchmelar.mrbc.API.POJO.Order;

/**
 * Created by jojko on 26.10.2016.
 */
public class Id {

    private String kind;
    private String appId;
    private String id;
    private Parent parent;
    private Boolean complete;

    /**
     * @return The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * @param kind The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * @return The appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * @param appId The appId
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The parent
     */
    public Parent getParent() {
        return parent;
    }

    /**
     * @param parent The parent
     */
    public void setParent(Parent parent) {
        this.parent = parent;
    }

    /**
     * @return The complete     */
    public Boolean getComplete() {
        return complete;
    }

    /**
     * @param complete The complete
     */
    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

}