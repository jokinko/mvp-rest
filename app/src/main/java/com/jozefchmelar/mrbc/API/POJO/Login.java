package com.jozefchmelar.mrbc.API.POJO;


/**
 * Created by jojko on 10.10.2016.
 */

public class Login {



        private Boolean success;
        private String message;
        private String token;

        /**
         *
         * @return
         * The success
         */
        public Boolean getSuccess() {
            return success;
        }

        /**
         *
         * @param success
         * The success
         */
        public void setSuccess(Boolean success) {
            this.success = success;
        }

        /**
         *
         * @return
         * The message
         */
        public String getMessage() {
            return message;
        }

        /**
         *
         * @param message
         * The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         *
         * @return
         * The token
         */
        public String getToken() {
            return token;
        }

        /**
         *
         * @param token
         * The token
         */
        public void setToken(String token) {
            this.token = token;
        }



    }

