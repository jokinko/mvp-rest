package com.jozefchmelar.mrbc.Contact;

import android.content.Context;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;

import java.util.List;

import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter;

/**
 * Created by jojko on 26.10.2016.
 */

public interface IContactPresenter {
    void loadListData();

    void setItemsListFromCache(String json);
}
