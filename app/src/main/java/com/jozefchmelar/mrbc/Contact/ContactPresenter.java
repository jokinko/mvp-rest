package com.jozefchmelar.mrbc.Contact;

import android.util.Log;

import com.google.gson.Gson;
import com.jozefchmelar.mrbc.API.POJO.Contact.Contact;
import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.API.RetrofitHolder;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by jojko on 26.10.2016.
 */

public class ContactPresenter implements IContactPresenter {

    private final String TAG = "ContactPresenter";
    private final IContactView contactView;
    private List<Item> listViewItems;


    public ContactPresenter(IContactView contactView) {
        this.contactView = contactView;
        this.contactView.initAdapter();
        this.contactView.setupActionBar();
    }

    @Override
    public void loadListData() {
        Log.d("LOADING_DAT", "Stahujem data");
        RetrofitHolder.getClient().getContacts().enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                listViewItems = response.body().getItems();
                contactView.setListData(listViewItems);
                contactView.disableRefreshAnimation();
                Log.d("LOADING_DAT", "Aplikujem stiahnute data");
                contactView.saveToSharedPref(convertListToJson(listViewItems));
            }
            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                contactView.displayToast("Error getting data");
                contactView.disableRefreshAnimation();
            }
        });
    }

    @Override
    public void setItemsListFromCache(String jsonList) {
        if (!(jsonList.isEmpty() || jsonList == null)) {
            Item[] items = new Gson().fromJson(jsonList, Item[].class);
            List<Item> listItems = Arrays.asList(items);
            contactView.setListData(listItems);
            Log.d("LOADING_DAT", "Aplikujem data z cache");
        }
    }

    private String convertListToJson(List list) {
        return new Gson().toJson(list);
    }

}

