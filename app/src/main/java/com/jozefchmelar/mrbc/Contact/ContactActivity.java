package com.jozefchmelar.mrbc.Contact;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jozefchmelar.mrbc.R;

public class ContactActivity extends AppCompatActivity implements  ContactListFragment.OnFragmentInteractionListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        //load fragment
        if(savedInstanceState==null){
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment list = new ContactListFragment();
        fragmentTransaction.add(R.id.fragment_contact,list); }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
