package com.jozefchmelar.mrbc.Contact;

import android.content.Context;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.inloop.simplerecycleradapter.SettableViewHolder;

/**
 * Created by jojko on 26.10.2016.
 */

public class ContactAdapter extends SettableViewHolder<Item> {
    @BindView(R.id.imageViewContactItemProfilePicture)
    ImageView imageViewProfilePicture;
    @BindView(R.id.textViewContactItemName)
    TextView textViewName;
    @BindView(R.id.textViewContactItemPhone)
    TextView textViewPhone;

    public ContactAdapter(@NonNull Context context, @LayoutRes int layoutRes, @NonNull ViewGroup parent) {
        super(context, layoutRes, parent);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(@NonNull Item data) {
        textViewName.setText(data.getName());
        textViewPhone.setText(data.getPhone());
        if (data.getPictureUrl() != null) {
            String url = "private api/" + data.getPictureUrl();
            //picasso is caching picture and resizing it totcorrect ratio, setting placeholder if loading fails.
            Picasso.with(super.itemView.getContext())
                    .load(url)
                    .placeholder(R.mipmap.fulatar)
                    .centerCrop()
                    .resize(imageViewProfilePicture.getDrawable().getMinimumWidth(), imageViewProfilePicture.getDrawable().getMinimumWidth())
                    .into(imageViewProfilePicture);

        } else {
            //piccaso would mess up views, and assign profile picture to item without any picture url.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageViewProfilePicture.setImageDrawable(itemView.getResources().getDrawable(R.mipmap.fulatar, itemView.getContext().getApplicationContext().getTheme()));
            } else {
                imageViewProfilePicture.setImageDrawable(itemView.getResources().getDrawable(R.mipmap.fulatar));
            }
        }
    }
}
