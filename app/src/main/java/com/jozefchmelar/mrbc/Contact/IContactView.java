package com.jozefchmelar.mrbc.Contact;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.IBaseView;

import java.util.List;

/**
 * Created by jojko on 26.10.2016.
 */

public interface IContactView extends IBaseView {
    void onItemInListClicked(Item item);

    void onActionBarPlusButtonClicked();

    void setListDataFromCache();

    void initAdapter();

    void setListData(List<Item> data);

    void disableRefreshAnimation();

    void saveToSharedPref(String json);
}
