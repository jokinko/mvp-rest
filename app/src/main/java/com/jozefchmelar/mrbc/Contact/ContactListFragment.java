package com.jozefchmelar.mrbc.Contact;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.Detail.DetailActivity;
import com.jozefchmelar.mrbc.NewContact.NewContactActivity;
import com.jozefchmelar.mrbc.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.inloop.simplerecycleradapter.ItemClickListener;
import eu.inloop.simplerecycleradapter.SettableViewHolder;
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter;


public class ContactListFragment extends Fragment implements IContactView, ItemClickListener<Item> {
    private static final String TAG = "ContactListFragment";
    private static final String SHARED_PREFERENCES = "Pref";
    private static final String SAVED_LIST = "Saved list on rotation";
    private static final String JSON_LIST = "JsonList";

    private ContactPresenter presenter;
    private SimpleRecyclerAdapter<Item> itemRecycleAdapter;
    private FrameLayout view;
    private FragmentActivity activity;
    private OnFragmentInteractionListener mListener;
    private Boolean downloadNewData = true;
    @BindView(R.id.recycleViewContacts)
    RecyclerView recycleViewContacts;
    @BindView(R.id.swipeRefreshContacts)
    SwipeRefreshLayout swipeRefresh;

    // Required empty public constructor
    public ContactListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (FrameLayout) inflater.inflate(R.layout.fragment_contact, container, false);
        ButterKnife.bind(this, view);
        activity = super.getActivity();
        presenter = new ContactPresenter(this);
        //loads data when I rotate screen
        if (savedInstanceState != null) {
            List<Item> savedList= (List<Item>) savedInstanceState.getSerializable(SAVED_LIST);
            Log.d("LOADING_DAT", "Obnova listu v ramci fragmentu");
            if(savedList!=null)
                setListData(savedList);
            else
                presenter.loadListData();
            savedInstanceState.remove(SAVED_LIST);
        } else {
            //try to cachce data
            setListDataFromCache();
            //load data
            presenter.loadListData();
        }
        //swipe to refresh.
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadListData();
            }
        });
        return view;
    }



    @Override
    public void initAdapter() {
        itemRecycleAdapter = new SimpleRecyclerAdapter<>(this, new SimpleRecyclerAdapter.CreateViewHolder<Item>() {
            @NonNull
            @Override
            public ContactAdapter onCreateViewHolder(final ViewGroup parent, final int viewType) {
                return new ContactAdapter(activity, R.layout.contact_item, parent);
            }
        });

        recycleViewContacts.setAdapter(itemRecycleAdapter);
        recycleViewContacts.setLayoutManager(new LinearLayoutManager(activity.getApplication()));
        recycleViewContacts.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_add:
                onActionBarPlusButtonClicked();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actionbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onItemInListClicked(Item item) {
        //intent to fill data in DetailActivity
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("Name", item.getName());
        intent.putExtra("Phone", item.getPhone());
        intent.putExtra("Id", item.getId());
        activity.startActivity(intent);
    }

    @Override
    public void onActionBarPlusButtonClicked() {
        Intent intent = new Intent(activity,NewContactActivity.class);
        startActivity(intent);
    }

    @Override
    public void setListDataFromCache() {
        //view gets data from android, passes it to presenter and presenter displays it.
        //get json from JS, then convert it to list, display it and right after that update list
        SharedPreferences sh = activity.getSharedPreferences(SHARED_PREFERENCES, activity.MODE_PRIVATE);
        if (!(sh.getString(JSON_LIST, "")).isEmpty()) { //don't bother loading from sp if it's first run
            String jsonList = sh.getString(JSON_LIST, "");
            presenter.setItemsListFromCache(jsonList);
        } else {
            swipeRefresh.setRefreshing(true);
        }
    }

    @Override
    public void saveToSharedPref(String json) {
        SharedPreferences sh = activity.getSharedPreferences(SHARED_PREFERENCES, activity.MODE_PRIVATE);
        sh.edit().putString(JSON_LIST, json).apply();
        Log.d("LOADING_DAT", "data ulozene do  SP fragment");
    }

    @Override
    public void setListData(List<Item> data) {
        //clears and sets data | true for notifyDataset
        itemRecycleAdapter.replaceItems(data, true);
    }

    @Override
    public void disableRefreshAnimation() {
        //stop animation when loading is done or fails.
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putBoolean("downloadNewData", false);
        ArrayList<Item> toSave = new ArrayList<>(itemRecycleAdapter.getItems());
        outState.putSerializable(SAVED_LIST, toSave);
    }

    @Override
    public void setupActionBar() {
        //top bar of app
        activity.setTitle(getString(R.string.contacts));
        ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_contacts);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void displayToast(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    //region fragment stuff
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(@NonNull Item item, @NonNull SettableViewHolder<Item> viewHolder, @NonNull View view) {
        onItemInListClicked(item);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    //endregion
}
