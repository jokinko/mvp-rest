package com.jozefchmelar.mrbc;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by jojko on 27.10.2016.
 */

public abstract interface IBaseView  {

    void setupActionBar();
    void displayToast(String message);

}
