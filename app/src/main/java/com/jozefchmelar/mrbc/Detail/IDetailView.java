package com.jozefchmelar.mrbc.Detail;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.API.POJO.Order.Order;
import com.jozefchmelar.mrbc.IBaseView;

import java.util.List;

/**
 * Created by jojko on 26.10.2016.
 */

public interface IDetailView extends IBaseView {

    void setListData(List<Order.Item> data);
}
