package com.jozefchmelar.mrbc.Detail;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.jozefchmelar.mrbc.API.POJO.Order.Order;
import com.jozefchmelar.mrbc.API.RetrofitHolder;

import eu.inloop.simplerecycleradapter.ItemClickListener;
import eu.inloop.simplerecycleradapter.SettableViewHolder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jojko on 26.10.2016.
 */

public class DetailPresenter implements IDetailPresenter, ItemClickListener<Order.Item> {

    private final IDetailView detailView;
    private final String TAG = "DetailPresenter";

    public DetailPresenter(IDetailView detailView) {
        this.detailView = detailView;
        this.detailView.setupActionBar();
    }

    @Override
    public void loadDataFromApi(String userId) {
        RetrofitHolder.getClient().getOrder(userId).enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                detailView.setListData(response.body().getItems());
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                detailView.displayToast("Error getting data");
            }
        });
    }

    @Override
    public void onItemClick(@NonNull Order.Item item, @NonNull SettableViewHolder<Order.Item> viewHolder, @NonNull View view) {

    }
}
