package com.jozefchmelar.mrbc.Detail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jozefchmelar.mrbc.API.POJO.Contact.Contact;
import com.jozefchmelar.mrbc.API.POJO.Order.Order;
import com.jozefchmelar.mrbc.Contact.ContactActivity;
import com.jozefchmelar.mrbc.NewContact.NewContactActivity;
import com.jozefchmelar.mrbc.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter;


public class DetailFragment extends Fragment implements IDetailView {

    @BindView(R.id.textViewDetailPhoneTitle)
    TextView textViewPhoneTitle;
    @BindView(R.id.textViewDetailPhoneNumber)
    TextView textViePhoneNumber;
    @BindView(R.id.recyclerDetail)
    RecyclerView recyclerDetail;

    private DetailPresenter presenter;
    private SimpleRecyclerAdapter<Order.Item> mRecyclerAdapter;
    private String name;
    private String phone;
    private String id;

    private FrameLayout view;
    private FragmentActivity activity;
    private OnFragmentInteractionListener mListener;

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = (FrameLayout) inflater.inflate(R.layout.fragment_detail_list, container, false);
        activity = super.getActivity();
        ButterKnife.bind(this, view);
        //get data from intent
        this.name = activity.getIntent().getExtras().get("Name").toString();
        this.phone = activity.getIntent().getExtras().get("Phone").toString();
        this.id = activity.getIntent().getExtras().get("Id").toString();
        textViewPhoneTitle.setText(R.string.phone);
        textViePhoneNumber.setText(phone);
        initAdapter();
        presenter = new DetailPresenter(this);
        presenter.loadDataFromApi(this.id);
        return view;
    }

    @OnClick(R.id.textViewDetailPhoneNumber)
    void dialNumber() {
        String str = textViePhoneNumber.getText().toString();
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + str));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                activity.onBackPressed();
                break;
        }
        return true;
    }



    private void initAdapter() {
        mRecyclerAdapter = new SimpleRecyclerAdapter<>(presenter, new SimpleRecyclerAdapter.CreateViewHolder<Order.Item>() {
            @NonNull
            @Override
            public DetailAdapter onCreateViewHolder(final ViewGroup parent, final int viewType) {
                return new DetailAdapter(activity, R.layout.detail_item, parent);
            }
        });
        recyclerDetail.setAdapter(mRecyclerAdapter);
        recyclerDetail.setLayoutManager(new LinearLayoutManager(activity));
        recyclerDetail.setHasFixedSize(true);
    }

    @Override
    public void setListData(List<Order.Item> data) {
        mRecyclerAdapter.addItems(data);
        mRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void setupActionBar() {
        activity.setTitle(name);
        ((AppCompatActivity) activity).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
        ((AppCompatActivity) activity).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);//to apply toolbar from fragment
    }

    @Override
    public void displayToast(String message) {
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }
}
