package com.jozefchmelar.mrbc.Detail;

/**
 * Created by jojko on 26.10.2016.
 */

public interface IDetailPresenter {
    void loadDataFromApi(String id);
}
