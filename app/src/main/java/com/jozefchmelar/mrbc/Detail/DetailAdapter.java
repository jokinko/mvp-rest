package com.jozefchmelar.mrbc.Detail;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jozefchmelar.mrbc.API.POJO.Order.Order;
import com.jozefchmelar.mrbc.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.inloop.simplerecycleradapter.SettableViewHolder;

/**
 * Created by jojko on 26.10.2016.
 */

public class DetailAdapter extends SettableViewHolder<Order.Item> {
    @BindView(R.id.textViewOrderItemName)
    TextView name;
    @BindView(R.id.textViewOrderItemCount)
    TextView count;

    public DetailAdapter(@NonNull Context context, @LayoutRes int layoutRes, @NonNull ViewGroup parent) {
        super(context, layoutRes, parent);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(@NonNull Order.Item data) {
        name.setText(data.getName());
        count.setText(data.getCount() + "x");
    }
}
