package com.jozefchmelar.mrbc.NewContact;

import android.util.Log;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.API.RetrofitHolder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by jojko on 26.10.2016.
 */

public class NewContactPresenter implements INewContactPresenter {

    private final String TAG = "NewContactPresenter";
    private final INewContactView contactView;

    public NewContactPresenter(INewContactView contactView) {
        this.contactView = contactView;
        this.contactView.setupActionBar();
    }

    @Override
    public boolean validateName(String name) {
        if (name.length() < 5) {
            contactView.setNameErrMsg();
            return false;
        }
        return true;
    }

    @Override
    public boolean validatePhone(String phone) {
        if (phone.length() < 5) {
            contactView.setPhoneNumberErrMsg();
            return false;
        }
        return true;

    }

    @Override
    public void sendDataToServer(String name, String phone) {
        if (validateName(name) && validatePhone(phone)) {
            RetrofitHolder.getClient().newContact(new Item(name, phone)).enqueue(new Callback<Item>() {
                @Override
                public void onResponse(Call<Item> call, Response<Item> response) {
                    contactView.displayToast(response.body().getName() + " with ID:" + response.body().getId() + " created");
                    contactView.returnToContactsActivity();
                }

                @Override
                public void onFailure(Call<Item> call, Throwable t) {
                    contactView.displayToast("Error with posting data");
                    Log.e(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }

}
