package com.jozefchmelar.mrbc.NewContact;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.jozefchmelar.mrbc.Contact.ContactActivity;
import com.jozefchmelar.mrbc.Detail.DetailFragment;
import com.jozefchmelar.mrbc.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewContactActivity extends AppCompatActivity implements NewContactFragment.OnFragmentInteractionListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new NewContactFragment();
        fragmentTransaction.add(R.id.fragment_new_contact,fragment);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
