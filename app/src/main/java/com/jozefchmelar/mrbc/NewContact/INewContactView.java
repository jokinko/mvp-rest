package com.jozefchmelar.mrbc.NewContact;

import com.jozefchmelar.mrbc.API.POJO.Contact.Item;
import com.jozefchmelar.mrbc.API.POJO.Contact.NewContact;
import com.jozefchmelar.mrbc.IBaseView;

/**
 * Created by jojko on 26.10.2016.
 */

public interface INewContactView extends IBaseView {

    void setNameErrMsg();

    void setPhoneNumberErrMsg();

    void returnToContactsActivity();//item is Contact item
}
