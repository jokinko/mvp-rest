package com.jozefchmelar.mrbc.NewContact;

/**
 * Created by jojko on 26.10.2016.
 */

public interface INewContactPresenter {

    boolean validateName(String login);
    boolean validatePhone(String phone);
    void sendDataToServer(String name,String phoneNumber);

}
