package com.jozefchmelar.mrbc.NewContact;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.jozefchmelar.mrbc.Contact.ContactActivity;
import com.jozefchmelar.mrbc.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewContactFragment extends Fragment implements INewContactView {


    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.editTextPhone)
    EditText editTextPhone;


    private NewContactPresenter presenter;

    private OnFragmentInteractionListener mListener;
    private FragmentActivity activity;

    public NewContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_contact, container, false);
        activity = super.getActivity();
        ButterKnife.bind(this, view);
        presenter = new NewContactPresenter(this);
        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setNameErrMsg() {
        editTextName.setError(getString(R.string.minimalNameLength));
    }

    @Override
    public void setPhoneNumberErrMsg() {
        editTextPhone.setError(getString(R.string.phoneNumberMinimalLengthMsg));
    }

    @Override
    public void returnToContactsActivity() {
        activity.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                returnToContactsActivity();
                break;
        }
        return true;
    }

    @Override
    public void setupActionBar() {
        activity.setTitle(getString(R.string.addNewContact));
        ((AppCompatActivity) activity).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
        ((AppCompatActivity) activity).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);//to apply toolbar from fragment

    }

    @Override
    public void displayToast(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.buttonAdd)
    void add() {
        String name = editTextName.getText().toString().trim();
        String phone = editTextPhone.getText().toString().trim();
        presenter.sendDataToServer(name, phone);
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
